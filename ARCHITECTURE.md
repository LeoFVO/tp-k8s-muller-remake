# Architecture du projet

## L'application web

L'application web sera contenu dans le dossier ./app/.
Nous utilisons ici un simple index.js mais nous pourrions y mettre tout type d'application.

## Image docker

Nous ferons une image docker de l'app pour faciliter son déploiment.
il nous suffit alors de faire:

- `docker build -t tp-k8s-muller .`
- `docker tag tp-k8s-muller:latest leodevsecops/tp-k8s-muller:tag`
- `docker push leodevsecops/tp-k8s-muller:tag`

Pensez à être bien login au dockerhub

## Kubernetes

Notre environnement et configuration kubernetes sera contenu dans le dossier ./kube/
Pour notre déploiment sur un cluster, il suffit de se connecter avec le KUBECONFIG et de faire:  
`kubectl apply -f ./kube/`

A noter que le pod utilise un tag de l'image spécifique et doit être mis à jour en cas de changement majeur ou mineur.

## A noter

Penser à bien lié l'addresse du ingress vers le nom de domaine souhaité, en l'occurence exam.do.local dans votre /etc/hosts
Pour cette application, l'ip sera : 206.189.245.81
