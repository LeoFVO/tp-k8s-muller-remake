const express = require('express');
const actuator = require('express-actuator');
const app = express();
const port = process.env.PORT ?? 8080;
console.log('port:', port);

// Health Check
app.use(actuator());

const buteur = process.env.BUTEUR_NAME ?? 'inconnu';
const buteur_b64_image = process.env.BUTEUR_B64_IMAGE ?? '';

app.get('/', (req, res) => {
  res.send(
    `<html>
    <head><link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"></link>
    
    <body class="grid place-items-center py-10">
    <h1 class="text-4xl text-indigo-600 p-4">C'était le 26 mai 93</h1>
    <h2 class="text-2xl text-indigo-500 p-2">Buteur : ${buteur}</h2>
    <img class="border-0 rounded-sm" src="data:image/png;base64, ${buteur_b64_image}" alt="${buteur}"/>
    </body>
    </html>`
  );
});

app.listen(port, () => {
  console.log(`App listening`);
  console.log("C'était le 26 mai 93");
  console.log('Buteur:', buteur);
});

// Graceful Shutdown
function handleShutdownGracefully() {
  console.log('Gracefully shutdown application.');
  app.close(() => {
    console.log('Server successfully closed.');
  });
}

process.on('SIGINT', handleShutdownGracefully);
process.on('SIGTERM', handleShutdownGracefully);
process.on('SIGHUP', handleShutdownGracefully);
